package com.linxo.controller;

import com.linxo.api.v1.Api;
import com.linxo.api.v1.core.ApiException;
import com.linxo.controller.utils.Environment;
import com.linxo.controller.utils.User;
import com.linxo.controller.utils.Utils;
import com.linxo.gwt.rpc.client.pfm.DeleteBankAccountAction;
import com.linxo.gwt.rpc.client.pfm.DeleteBankAccountResult;
import com.linxo.logging.Logger;
import com.linxo.logging.LoggerFactory;
import retrofit2.Response;

import java.io.IOException;

public class DeleteAccountTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteAccountTest.class);

    private static Api api;

    private static String groupId = "10053";
    private static String accountId = "10053";


    public static void main(String[] args) throws Exception {
        Environment environment = Utils.selectEnvironment();

        User user = Utils.selectUser(environment);

        api = new Api(Utils.start(user, environment), new Api.ApiListener() {
            @Override
            public void onStarted() {
                try {

                    LOGGER.debug("Api client démarrée sur l'environement [{}]", environment.getName());

                    Utils.authorizeDevice(api, user);

                    LOGGER.debug("Delete account");
                    Response<DeleteBankAccountResult> deleteBankAccountResultResponse = deleteBankAccount(api);
                    if (deleteBankAccountResultResponse == null
                        || !deleteBankAccountResultResponse.isSuccessful()
                        || deleteBankAccountResultResponse.body() == null) {
                        LOGGER.debug("Cannot delete bank account");
                        return;
                    }
                }
                catch (Exception e) {
                    LOGGER.debug("Test failed : " + e);
                }
            }

            @Override
            public void onException(final ApiException e) {
                //on API exception
                LOGGER.debug("Exception appeared on api config : " + e.getMessage());
            }
        });
    }

    private static Response<DeleteBankAccountResult> deleteBankAccount(Api api)
            throws IOException {
        return api.getApi().send(new DeleteBankAccountAction(
                groupId,
                accountId
        )).execute();
    }
}
