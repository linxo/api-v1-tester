package com.linxo.controller;

import com.linxo.api.v1.Api;
import com.linxo.api.v1.core.ApiException;
import com.linxo.controller.utils.Environment;
import com.linxo.controller.utils.User;
import com.linxo.controller.utils.Utils;
import com.linxo.gwt.rpc.client.pfm.GetTransactionAction;
import com.linxo.gwt.rpc.client.pfm.GetTransactionResult;
import com.linxo.logging.Logger;
import com.linxo.logging.LoggerFactory;
import retrofit2.Response;

import java.io.IOException;

public class GetTransactionTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(GetTransactionTest.class);


    private static Api api;

    public static void main(String[] args) throws Exception {
        Environment environment = Utils.selectEnvironment();

        User user = Utils.selectUser(environment);

        api = new Api(Utils.start(user, environment), new Api.ApiListener() {
            @Override
            public void onStarted() {
                try {

                    LOGGER.debug("Api client démarrée sur l'environement [{}]", environment.getName());

                    Utils.authorizeDevice(api, user);

                    String transactionId = Utils.getInput("Identifiant de la transaction ?");

                    LOGGER.debug("Récupération de la transaction [" + transactionId + "]");

                    Response<GetTransactionResult> getTransactionResultResponse = getTransaction(transactionId, api);
                    if (getTransactionResultResponse == null
                        || !getTransactionResultResponse.isSuccessful()
                        || getTransactionResultResponse.body() == null) {
                        LOGGER.debug("Impossible de récupérer la transaction ["+transactionId+"]");
                        return;
                    }
                    Utils.displayTransaction(getTransactionResultResponse.body().getTransaction());
                }
                catch (Exception e) {
                    LOGGER.debug("Echec du test  : " + e);
                }
            }

            @Override
            public void onException(final ApiException e) {
                //on API exception
                LOGGER.debug("Exception lors de la config de l'api: " + e.getMessage());
            }
        });
    }

    private static Response<GetTransactionResult> getTransaction(String transactionId, Api api)
            throws IOException {
        return api.getApi().send(new GetTransactionAction(transactionId, true)).execute();
    }
}
