package com.linxo.controller;

import com.linxo.api.v1.Api;
import com.linxo.api.v1.core.ApiException;
import com.linxo.controller.utils.Environment;
import com.linxo.controller.utils.EnvironmentUtils;
import com.linxo.controller.utils.User;
import com.linxo.controller.utils.UserUtils;
import com.linxo.controller.utils.Utils;
import com.linxo.gwt.rpc.client.dto.tx.TransactionInfo;
import com.linxo.gwt.rpc.client.pfm.GetTransactionsAction;
import com.linxo.gwt.rpc.client.pfm.GetTransactionsResult;
import com.linxo.gwt.rpc.client.pfm.TransactionStatus;
import com.linxo.logging.Logger;
import com.linxo.logging.LoggerFactory;
import retrofit2.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GetTransactionsTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(GetTransactionsTest.class);

    private static Api api;

    public static void main(String[] args) throws Exception {
        Environment environment = EnvironmentUtils.getQa();

        User user = UserUtils.getByName("dev", environment);

        api = new Api(Utils.start(user, environment), new Api.ApiListener() {
            @Override
            public void onStarted() {
                try {

                    LOGGER.debug("Api client démarrée sur l'environement [{}]", environment.getName());

                    Utils.authorizeDevice(api, user);

                    String accountId = Utils.getInput("Identifiant du compte ?");

                    LOGGER.debug("Récupération des transactions pour le compte [" + accountId + "]");
                    Response<GetTransactionsResult> getTransactionResultResponse = getTransactions(Long.valueOf(accountId), api);
                    if (getTransactionResultResponse == null
                        || !getTransactionResultResponse.isSuccessful()
                        || getTransactionResultResponse.body() == null) {
                        LOGGER.debug("Impossible de récupérer les transactions pour le compte ["+accountId+"]");
                        return;
                    }

                    List<TransactionInfo> transactionInfoList = getTransactionResultResponse.body().getTransactions();
                    if (transactionInfoList == null || transactionInfoList.isEmpty()) {
                        LOGGER.debug("Pas de transaction à afficher pour le compte [" + accountId + "]");
                        return;
                    }

                    for (TransactionInfo transactionInfo : transactionInfoList) {
                        Utils.displayTransaction(transactionInfo);
                    }
                }
                catch (Exception e) {
                    LOGGER.debug("Echec du test  : " + e);
                }
            }

            @Override
            public void onException(final ApiException e) {
                //on API exception
                LOGGER.debug("Exception lors de la config de l'api: " + e.getMessage());
            }
        });
    }

    private static Response<GetTransactionsResult> getTransactions(Long accountId, Api api)
            throws IOException {
        GetTransactionsAction action = new GetTransactionsAction();
        action.setAccountId(accountId);
        action.setStartRow(0);
        action.setNumRows(10);
        action.setIncludedStatuses(new ArrayList<com.linxo.gwt.rpc.client.pfm.TransactionStatus>(){{add(TransactionStatus.PENDING);}});

        LOGGER.debug("Récupération des transactions pour l'action [{}]", action);

        return api.getApi().send(action).execute();
    }
}
