package com.linxo.controller;

import com.linxo.api.v1.Api;
import com.linxo.api.v1.core.ApiException;
import com.linxo.controller.utils.Environment;
import com.linxo.controller.utils.User;
import com.linxo.controller.utils.Utils;
import com.linxo.gwt.rpc.client.auth.AuthorizeDeviceAction;
import com.linxo.gwt.rpc.client.auth.AuthorizeDeviceResult;
import com.linxo.gwt.rpc.client.dto.device.AppInfo;
import com.linxo.gwt.rpc.client.pfm.sync.Credential;
import com.linxo.logging.Logger;
import com.linxo.logging.LoggerFactory;
import retrofit2.Response;
import sun.util.locale.LocaleUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class B4BAuthDeviceTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(B4BAuthDeviceTest.class);

    private static Api api;

    public static void main(String[] args)
            throws Exception {
        Environment environment = Utils.selectEnvironment();

        User user = Utils.selectUser(environment);

        api = new Api(Utils.start(user, environment), new Api.ApiListener() {
            @Override
            public void onStarted() {
                LOGGER.debug("Api client démarrée sur l'environement [{}]", environment.getName());

                List<Long> spentTimes = new CopyOnWriteArrayList<>();

                int nbCalls = 1;

                String strNbCalls = Utils.getInput("Nombre d'appels");
                if (LocaleUtils.isAlphaNumericString(strNbCalls)) {
                    nbCalls = Integer.valueOf(strNbCalls);
                }

                ExecutorService executorService = new ThreadPoolExecutor(nbCalls,
                                                                         nbCalls,
                                                                         100000,
                                                                         TimeUnit.SECONDS,
                                                                         new LinkedBlockingQueue<>());

                AtomicInteger count = new AtomicInteger();
                try {

                    LOGGER.debug("Api client démarrée");

                    for (int i=0; i<nbCalls; i++) {

                        int finalI = i;
                        executorService.submit(() -> {

                            long start = new Date().getTime();
                            try {
                                LOGGER.debug(finalI + " - " + LocalDateTime.now() + " - 'Authorize device' sans credentials pour l'utilisateur " + user.getEmail());
                                authorizeDevice(api, user, null);
                            }
                            catch (Exception e) {
                                LOGGER.debug(LocalDateTime.now() + " - Authorize device failed with exception " + e);
                            }

                            LOGGER.debug("");
                            LOGGER.debug("");
                            LOGGER.debug("");

                            try {
                                String clientIdentifier = "b4b_token";
                                String authorizationCode = "auth_code";
                                LOGGER.debug(finalI + " - " + LocalDateTime.now() + " - 'Authorize device' avec l'identifiant client " + authorizationCode +
                                                   " et le code d'auhtorisation " + authorizationCode + " pour l'utilisateur " + user.getEmail());
                                ArrayList<Credential> credentials = new ArrayList<>();
                                credentials.add(new Credential("9804", "credentials.b4b.identifier", clientIdentifier));
                                credentials.add(new Credential("9805", "credentials.bforbank.webview", authorizationCode));
                                authorizeDevice(api, user, credentials);
                            }
                            catch (Exception e) {
                                LOGGER.debug(LocalDateTime.now() + " - Authorize device failed with exception " + e);
                            }

                            LOGGER.debug("");
                            LOGGER.debug("");
                            LOGGER.debug("");

                            try {
                                LOGGER.debug(finalI + " - " + LocalDateTime.now() + " - 'Authorize device' avec acceptation des CGUs pour l'utilisateur " + user.getEmail());
                                ArrayList<Credential> credentials = new ArrayList<>();
                                credentials.add(new Credential("9806", "credentials.bforbank.cgu", "true"));
                                authorizeDevice(api, user, credentials);
                            }
                            catch (Exception e) {
                                LOGGER.debug(LocalDateTime.now() + " - Authorize device failed with exception " + e);
                            }

                            LOGGER.debug("");
                            LOGGER.debug("");
                            LOGGER.debug("");

                            long spentTime = new Date().getTime() - start;
                            LOGGER.debug(LocalDateTime.now() + " - " + finalI + " - Temps d'execution : " + spentTime + " ms");
                            spentTimes.add(spentTime);
                            count.incrementAndGet();
                        });
                    }
                }
                catch (Exception e) {
                    LOGGER.debug("Echec du test  : " + e);
                }

                while (count.get() < nbCalls) {
                    try {
                        Thread.sleep(100);
                    }
                    catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                long totalSpentTime = spentTimes.stream().mapToLong(time -> time).sum();
                LOGGER.debug("Temps moyen d'execution" + (totalSpentTime / nbCalls));
            }

            @Override
            public void onException(final ApiException e) {
                //on API exception
                LOGGER.debug("Exception lors de la requete avec le message [" + e.getMessage() + "]");
            }
        });
    }

    private static void authorizeDevice(Api api, User user, ArrayList<Credential> credentials)
            throws Exception {

        AuthorizeDeviceAction authorizeDeviceAction = new AuthorizeDeviceAction(
                "deviceFamily",
                "deviceType",
                user.getDeviceId(),
                user.getEmail(),
                user.getPassword(),
                "deviceTest",
                new AppInfo("com.linxo.payments.client",
                            "1.0.0",
                            false,
                            ""));

        authorizeDeviceAction.setCredentials(credentials);

        Response<AuthorizeDeviceResult> authorizeDeviceResultResponse = api.getApi().send(authorizeDeviceAction).execute();

        AuthorizeDeviceResult authorizeDeviceResult = authorizeDeviceResultResponse.body();
        if (authorizeDeviceResult == null) {
            LOGGER.debug("Failed to authorize device");
            throw new Exception();
        }
        LOGGER.debug(LocalDateTime.now() + " - Return result with status [" + authorizeDeviceResult.getStatus() + "] and message [" + authorizeDeviceResultResponse.message() + "]");
    }
}