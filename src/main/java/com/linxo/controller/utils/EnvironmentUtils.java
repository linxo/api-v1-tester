package com.linxo.controller.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class EnvironmentUtils {
    private static List<Environment> environments;

    private static Environment qa;
    private static Environment qa_b4b;
    private static Environment local;

    static {
        environments = new ArrayList<>();

        local = new Environment("Local",
                                            "https://pfm.192.168.99.100.xip.io");

        qa = new Environment("QA",
                                         "https://linxo1-qa.linxo.com");

        qa_b4b = new Environment("QA-B4B",
                             "https://pfm-bforbank.qa.linxogroup.net");

        environments.add(local);
        environments.add(qa);
        environments.add(qa_b4b);
    }

    static Environment selectEnvironment() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Selectionner votre environement: ");
        int i = 0;
        Iterator<Environment> environmentIterator = environments.iterator();
        while (environmentIterator.hasNext()) {
            stringBuilder.append(i).append("[").append(environmentIterator.next().getName()).append("]");
            if (environmentIterator.hasNext()) {
                stringBuilder.append(", ");
            }
            i++;
        }

        String env = Utils.getInput(stringBuilder.toString() + " ");
        if (Utils.isNumeric(env) && Integer.valueOf(env) >= 0 && Integer.valueOf(env) < environments.size()) {
            return environments.get(Integer.valueOf(env));
        }
        return null;
    }

    public static Environment getQa() {
        return qa;
    }

    public static Environment getLocal() {
        return local;
    }

    public static Environment getQaB4B() {
        return qa_b4b;
    }
}
