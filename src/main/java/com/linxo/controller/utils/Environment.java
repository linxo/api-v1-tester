package com.linxo.controller.utils;

import java.util.StringJoiner;

public class Environment {
    private String name;
    private String apiUrl;

    public Environment(String name, String apiUrl) {
        this.name = name;
        this.apiUrl = apiUrl;
    }

    public String getName() {
        return name;
    }

    public String getApiUrl() {
        return apiUrl;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Environment.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .add("apiUrl='" + apiUrl + "'")
                .toString();
    }
}
