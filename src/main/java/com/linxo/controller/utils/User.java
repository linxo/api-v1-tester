package com.linxo.controller.utils;

import java.util.StringJoiner;

public class User {
    private String name;
    private String email;
    private String password;
    private String clientId;
    private String clientSecret;
    private String deviceId;
    private Environment environment;

    public User(String name,
                String email,
                String password,
                String deviceId,
                String clientId,
                String clientSecret,
                Environment environment) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.deviceId = deviceId;
        this.environment = environment;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getClientId() {
        return clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public Environment getEnvironment() {
        return environment;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", User.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .add("email='" + email + "'")
                .add("password='" + password + "'")
                .add("clientId='" + clientId + "'")
                .add("clientSecret='" + clientSecret + "'")
                .add("deviceId='" + deviceId + "'")
                .add("environment=" + environment)
                .toString();
    }
}
