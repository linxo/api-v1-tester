package com.linxo.controller.utils;

import com.linxo.api.v1.Api;
import com.linxo.api.v1.core.ApiConfig;
import com.linxo.api.v1.core.ApiConfigBuilder;
import com.linxo.controller.ListAccountsTest;
import com.linxo.crypto.lib.Encryption;
import com.linxo.crypto.lib.LinxoCryptoException;
import com.linxo.gwt.rpc.client.auth.AuthorizeDeviceAction;
import com.linxo.gwt.rpc.client.auth.AuthorizeDeviceResult;
import com.linxo.gwt.rpc.client.dto.device.AppInfo;
import com.linxo.gwt.rpc.client.dto.tx.TransactionInfo;
import com.linxo.gwt.rpc.client.pfm.sync.Credential;
import com.linxo.logging.Logger;
import com.linxo.logging.LoggerFactory;
import org.bouncycastle.openpgp.PGPException;
import retrofit2.Response;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Scanner;

public class Utils {

    private static final Logger LOGGER = LoggerFactory.getLogger(ListAccountsTest.class);

    public static Environment selectEnvironment()
            throws Exception {
        Environment environment = EnvironmentUtils.selectEnvironment();
        if (environment == null) {
            String message = "Mauvais environnement selectionné";
            LOGGER.debug(message);
            throw new Exception(message);
        }
        return environment;
    }

    public static User selectUser(Environment environment)
            throws Exception {
        User user = UserUtils.selectUser(environment);
        if (user == null) {
            String message = "Mauvais utilisateur selectionné";
            LOGGER.debug(message);
            throw new Exception(message);
        }
        return user;
    }

    public static ApiConfig start(User user, Environment environment) {

        return new ApiConfigBuilder()
                .apiKey(user.getClientId())
                .connectTimeout(1000000)
                .readTimeout(1000000)
                .writeTimeout(1000000)
                .apiSecret(Utils.SecretEncoder.encode(user.getClientSecret()))
                .apiUrl(environment.getApiUrl())
                .apiVersion("1.10.0")
                .appVersion("1.0.0")
                .versionCode("1.0.0")
                .debug(false)
                .disableCheckSSL()
                .build();
    }

    public static class SecretEncoder {

        private SecretEncoder() {
        }

        static String encode(String s) {
            int size = s.length();
            byte[] cString = s.getBytes();

            byte[] buffer = new byte[size];
            for (int i = 0; i < size; i++) {
                buffer[size-1-i] = cString[i];
            }

            byte[] buffer2 = new byte[size];
            for(int i = 0; i < size; i += 2) {
                buffer2[i] = (byte) (buffer[i+1]-1);
                buffer2[i+1] = (byte) (buffer[i]+1);
            }

            return new String(buffer2);
        }
    }

    public static String encryptSecret(String publicKey, String secret)
            throws LinxoCryptoException, IOException, PGPException {
        return Encryption.encryptLikeJavaScript(secret, publicKey);
    }

    public static String getInput(String message) {
        String command;
        Scanner in = new Scanner(System.in);
        in.useDelimiter(System.lineSeparator() + "|\n");
        do {
            System.out.print(message);
            command = in.nextLine();
            if (!command.isEmpty()) {
                break;
            }
        } while (true);
        return command;
    }



    public static void displayTransaction(TransactionInfo transactionInfo) {
        LOGGER.debug("********************");
        LOGGER.debug("* Transaction      *");
        LOGGER.debug("********************");
        LOGGER.debug("Bank account id: {}", transactionInfo.bankAccountId);
        LOGGER.debug("Id: {}", transactionInfo.getId());
        LOGGER.debug("Amount: {}", transactionInfo.amount);
        LOGGER.debug("Original memo: {}", transactionInfo.originalMemo);
        LOGGER.debug("Remittance info: {}", transactionInfo.remittanceInformation);
        LOGGER.debug("********************");
    }

    public static void authorizeDevice(Api api, User user)
            throws Exception {

        LOGGER.debug("Authorize device for user [{}]", user.getName());

        AuthorizeDeviceAction authorizeDeviceAction = new AuthorizeDeviceAction(
                "deviceFamily",
                "deviceType",
                user.getDeviceId(),
                user.getEmail(),
                user.getPassword(),
                "deviceTest",
                new AppInfo("com.linxo.payments.client",
                            "1.0.0",
                            false,
                            ""));

        ArrayList<Credential> credentials = new ArrayList<>();
        credentials.add(new Credential("9801", "credentials.bforbank.cgu", "true"));
        authorizeDeviceAction.setCredentials(credentials);

        Response<AuthorizeDeviceResult> authorizeDeviceResultResponse = api.getApi().send(authorizeDeviceAction).execute();

        AuthorizeDeviceResult authorizeDeviceResult = authorizeDeviceResultResponse.body();
        if (authorizeDeviceResult == null || authorizeDeviceResult.getToken() == null) {
            LOGGER.debug("Cannot retrieve token");
            throw new Exception();
        }

        LOGGER.debug(LocalDateTime.now() + " - " + authorizeDeviceResultResponse.message());
    }

    static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }
}
