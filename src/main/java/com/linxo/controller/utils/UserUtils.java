package com.linxo.controller.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class UserUtils {
    private static List<User> users;

    static {
        users = new ArrayList<>();

        User jeromeLocal = new User("jerome",
                               "jerome-r@linxo.com",
                               "P@ssword19",
                               "ffa8164c-6df0-4aee-a342-cb47de8f5164",
                               "c749c77e-2af8-45ad-91a5-c328a24e0f8d",
                               "186426286e7f7a241368",
                               EnvironmentUtils.getLocal());

        User jeromeQA = new User("jerome",
                                    "jerome-r@linxo.com",
                                    "P@ssword18",
                                    "6274d06f-cd6c-423f-8f7b-196bb093e078",
                                    "c749c77e-2af8-45ad-91a5-c328a24e0f8d",
                                    "186426286e7f7a241368",
                                    EnvironmentUtils.getQa());

        User devQAB4B = new User("dev",
                                 "dev@linxo.com",
                                 "Passw0rd",
                                 "afa8164c-6df0-4aee-a342-cb47de8f5164",
                                 "c749c77e-2af8-45ad-91a5-c328a24e0f8d",
                                 "186426286e7f7a241368",
                                 EnvironmentUtils.getQaB4B());

        User devLocal = new User("dev",
                        "dev@linxo.com",
                            "Passw0rd",
                            "afa8164c-6df0-4aee-a342-cb47de8f5164",
                            "c749c77e-2af8-45ad-91a5-c328a24e0f8d",
                            "186426286e7f7a241368",
                            EnvironmentUtils.getLocal());

        User devQA = new User("dev",
                            "dev@linxo.com",
                            "Passw0rd",
                            "afa8164c-6df0-4aee-a342-cb47de8f5164",
                            "c749c77e-2af8-45ad-91a5-c328a24e0f8d",
                            "186426286e7f7a241368",
                            EnvironmentUtils.getQa());

        users.add(jeromeLocal);
        users.add(jeromeQA);
        users.add(devLocal);
        users.add(devQA);
        users.add(devQAB4B);
    }

    static User selectUser(Environment environment) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Selectionner votre utilisateur : ");
        int i = 0;
        Iterator<User> userIterator = users.iterator();
        List<User> usersSubList = new ArrayList<>();
        while (userIterator.hasNext()) {
            User user = userIterator.next();
            if (user.getEnvironment().equals(environment)) {
                usersSubList.add(user);
                stringBuilder.append(i).append("[").append(user.getName()).append(":").append(user.getEmail()).append("]");
                if (userIterator.hasNext()) {
                    stringBuilder.append(", ");
                }
                i++;
            }
        }

        String user = Utils.getInput(stringBuilder.toString());
        if (Utils.isNumeric(user) && Integer.valueOf(user) >= 0 && Integer.valueOf(user) < usersSubList.size()) {
            return usersSubList.get(Integer.valueOf(user));
        }
        return null;
    }

    public static User getByName(String name, Environment environment)
            throws Exception {
        for (User user : users) {
            if (user.getName().equals(name) && user.getEnvironment().equals(environment)) {
                return user;
            }
        }
        throw new Exception("Cannot find user with name ["+name+"] for environment ["+environment+"]");
    }
}
