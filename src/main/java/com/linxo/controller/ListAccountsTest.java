package com.linxo.controller;

import com.linxo.api.v1.Api;
import com.linxo.api.v1.core.ApiException;
import com.linxo.controller.utils.Environment;
import com.linxo.controller.utils.User;
import com.linxo.controller.utils.Utils;
import com.linxo.crypto.lib.LinxoCryptoException;
import com.linxo.data.shared.client.pfm.keypairs.KeyPairType;
import com.linxo.gwt.rpc.client.dto.keys.KeyInfo;
import com.linxo.gwt.rpc.client.keys.CreateKeyAction;
import com.linxo.gwt.rpc.client.keys.CreateKeyResult;
import com.linxo.gwt.rpc.client.pfm.sync.Credential;
import com.linxo.gwt.rpc.client.pfm.sync.ListAccountsInFinancialInstitutionAction;
import com.linxo.gwt.rpc.client.pfm.sync.ListAccountsInFinancialInstitutionResult;
import com.linxo.logging.Logger;
import com.linxo.logging.LoggerFactory;
import org.bouncycastle.openpgp.PGPException;
import retrofit2.Response;

import java.io.IOException;
import java.util.ArrayList;

public class ListAccountsTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ListAccountsTest.class);

    private static Api api;

    public static void main(String[] args) throws Exception {
        Environment environment = Utils.selectEnvironment();

        User user = Utils.selectUser(environment);

        api = new Api(Utils.start(user, environment), new Api.ApiListener() {
            @Override
            public void onStarted() {
                LOGGER.debug("Api client démarrée sur l'environement [{}]", environment.getName());

                try {
                    Utils.authorizeDevice(api, user);
                }
                catch (Exception e) {
                    LOGGER.error("Bad result", e);
                    return;
                }

                LOGGER.debug("Create a PGP key");
                Response<CreateKeyResult> createKeyResultResponse;
                try {
                    createKeyResultResponse = api.getApi().send(new CreateKeyAction(
                            KeyPairType.PGP
                    )).execute();
                }
                catch (IOException e) {
                    LOGGER.error("Cannot create key", e);
                    return;
                }

                LOGGER.debug(createKeyResultResponse.message());

                CreateKeyResult createKeyResult = createKeyResultResponse.body();
                if (createKeyResult == null || createKeyResult.getKey() == null) {
                    LOGGER.error("Cannot create key");
                    return;
                }
                KeyInfo keyInfo = createKeyResult.getKey();

                ArrayList<Credential> credentialList = new ArrayList<>();
                credentialList.add(new Credential("300", "credentials.linxo.identifier", "dev"));
                try {
                    credentialList.add(new Credential("301", "credentials.linxo.secret", Utils.encryptSecret(keyInfo.getPublicKey(), "dev")));
                }
                catch (LinxoCryptoException | IOException | PGPException e) {
                    LOGGER.error("Cannot encrypt secret", e);
                    return;
                }

                credentialList.add(new Credential("302", "credentials.linxo.home_url", "https://www.dropbox.com/s/u2abg9z7eq5wyvd/SavingAccounts.txt?dl=1"));
                try {
                    String accountGroupId = "10001";
                    //String accountGroupId = "51957";
                    LOGGER.debug("Send a GetFinancialInstitutionAndListAccountsAction with account group id ["+accountGroupId+"]");
                    ListAccountsInFinancialInstitutionAction action = new ListAccountsInFinancialInstitutionAction(206L,
                                                                                                                   credentialList,
                                                                                                                   Boolean.FALSE,
                                                                                                                   keyInfo.getId(),
                                                                                                                   Boolean.TRUE);
                    //action.setSharedSecret("F71BA8A54C");
                    Response<ListAccountsInFinancialInstitutionResult> response = api.getApi().send(action).execute();
                    if (response != null && response.isSuccessful()) {
                        LOGGER.debug("Result : " + response.body());
                    } else {
                        LOGGER.error("Bad result");
                    }
                }
                catch (IOException e) {
                    LOGGER.error("Error sending a GetFinancialInstitutionAndListAccountsAction.");
                    LOGGER.error(e.getMessage());
                }
            }

            @Override
            public void onException(final ApiException e) {
                //on API exception
                LOGGER.debug("Exception appeared on api config : " + e.getMessage());
            }
        });
    }
}
